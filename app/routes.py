#!/usr/bin/python3

from flask import Flask, render_template, redirect
from subprocess import check_output, check_call
from app import app
from form import LoginForm
from config import Config

# En aquest arxiu aniran totes les rutes de cada URL

app = Flask(__name__)
app.config.from_object(Config)

# Un usuari hardocejat
user = {'username': 'Naomi'}

# Torna un arxiu html ./templates/index.html
@app.route("/")
def cosas():
    return render_template('index.html', user=user)

# Retorna un formulari de login ./rtemplates/login
@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        flash('Login requested for user {}, remember_me={}'.format(
            form.username.data, form.remember_me.data))
        return redirect('/')
    return render_template('login.html', title='Sign In', form=form)

# Retorna el nom de la variable user, més adalt        
@app.route("/hello")
def hello():
        return '''
<html>
    <head>
        <title>Home Page - Microblog</title>
    </head>
    <body>
        <h1>Hello, ''' + user['username'] + '''!</h1>
    </body>
</html>'''

# Pàgina per testejar els condicionals 
@app.route("/test")
def test():
    return render_template('test.html', title='test')

# Links a login i a l'índex
@app.route("/base")
def base():
    return render_template('base.html', title='test')

# Per a que funcioni en un docket i s'actualitzi automàticament al guardar
app.run(host="0.0.0.0", debug=True)